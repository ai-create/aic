# Installation

```
$ curl -s (--insecure) https://aic.ai-create.net/install | bash
```

## In manual

```
$ cd ~
$ git clone https://k-aso@bitbucket.org/ai-create/aic.git
$ mv aic .aic
$ cd ~/.aic
$ find ~/.aic/aic -type f | xargs chmod 755
$ echo -n -e 'export PATH=$PATH:/Users/<UESR NAME>/.aic\n' >> ~/.zprofile
```


# Commands

You can check the list of commands with the Help option.

```
$ aic help
```
